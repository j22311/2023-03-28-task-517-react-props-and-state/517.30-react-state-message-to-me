import { Component } from "react";
import like from '../../../assets/images/like.png'
class OutputContent extends Component {
    render() {
        let {outputMessageProps,likeDisplayProps} = this.props
        return (
        <>
                 <div className='row mt-2'>

                 {outputMessageProps.map((element,index) => {
                    return <p key={index}>{element}</p>
                 })}
                    
                </div>

                <div className='row mt-2'>
                <div className='col-12'>
                {likeDisplayProps ? <img src={like}  width ='200px;'></img> : <></> }
                </div>
                </div>
        </>
        )
    }
}

export default OutputContent;