import { Component } from "react";

class InputContent extends Component{
    onInputchangeHandler = (event) => {
        console.log("nhập message...");
        console.log(event.target.value); 
        let value = event.target.value;

        this.props.inputMessageChangHandleProps(value);
    }
    onButtonClickHandler = () => {
        console.log("Ấn nút gửi thông điệp ")
        this.props.outputMessageChangHandleProps();
    }
    render() {
        return (
        <>
            <div className='row mt-2'>
                <label className='label'>Message cho 12 tháng tới</label>
            </div>
            <div className='row mt-2'>
                <div className='col-12'>
                    <input className='form-control' value={this.props.inputMessageProps}  placeholder="nhập email ở đây" onChange={this.onInputchangeHandler} style={{width: "600px",margin: "0 auto"}}></input>
                </div>
            </div>
            <div className='row mt-2'>
                <div className='col-12'>
                    <button className="btn btn-info" onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                </div>
            </div>
            
            </>
        )
    }
}

export default InputContent;