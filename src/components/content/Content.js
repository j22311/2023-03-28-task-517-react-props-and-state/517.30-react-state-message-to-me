import { Component } from "react";

import InputContent from "./input/inputContent";
import OutputContent from "./output/outputContent";
class Content extends Component {
    constructor(props){
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }
    inputMessageChangHandle = (value) => {
        this.setState({
            inputMessage: value
        })
    }
    outputMessageChangHandle = () => {
        this.setState({
            outputMessage: [...this.state.outputMessage, this.state.inputMessage],
            likeDisplay: true
        })
    }
    render() {
        return (
            <>
                <InputContent inputMessageProps={this.state.inputMessage}
                    inputMessageChangHandleProps = {this.inputMessageChangHandle}
                    outputMessageChangHandleProps = {this.outputMessageChangHandle}
                />
                <OutputContent outputMessageProps={this.state.outputMessage} likeDisplayProps={this.state.likeDisplay}/> 
       
            </>
        )
    }
}

export default Content;